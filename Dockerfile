FROM ruby:2.5.0

WORKDIR /srv/geo_bot
ENTRYPOINT ["bin/bot"]

# Set defaults
ENV RAILS_ENV=production \
    RACK_ENV=production

# Caching gems
ADD Gemfile Gemfile.lock ./

RUN apk update
RUN apk add build-base git \
            libxml2-dev \
            libxslt-dev \
            libffi-dev \
            postgresql-dev \
            build-base

RUN bundle install

ADD . ./

ARG git_revision='unknown'
ARG git_branch='unknown'
ARG build_number='unknown'
ARG build_timestamp='unknown'

LABEL build.git-revision=$git_revision \
      build.git-branch=$git_branch \
      build.ci-number=$build_number \
      build.timestamp=$build_timestamp

ENV BUILD_REVISION=$git_revision \
    BUILD_BRANCH=$git_branch \
    BUILD_NUMBER=$build_number \
    BUILD_TIMESTAMP=$build_timestamp
