DOCKER_REGISTRY ?= hub.docker.com
DOCKER_IMAGE ?= geo-bot
DOCKER_IMAGE_BUILD ?= 0.1
DOCKER_IMAGE_TAG ?= test-build
NORMALIZED_DOCKER_IMAGE_TAG = $(shell echo ${DOCKER_IMAGE_TAG} | tr / -)
DOCKERFILE ?= Dockerfile

GIT_REVISION ?= $(shell git rev-parse HEAD)
GIT_BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)

BUILD_NUMBER ?= develop
BUILD_TIMESTAMP ?= $(shell date -u +"%Y-%m-%dT%H:%M:%S")

build:
	docker build --rm --force-rm --pull \
							 --build-arg git_branch=${GIT_BRANCH} \
							 --build-arg git_revision=${GIT_REVISION} \
							 --build-arg build_number=${BUILD_NUMBER} \
							 --build-arg build_timestamp=${BUILD_TIMESTAMP} \
							 -f ${DOCKERFILE} \
							 -t ${DOCKER_REGISTRY}/${DOCKER_IMAGE}:${DOCKER_IMAGE_BUILD} .

tag:
	docker tag ${DOCKER_REGISTRY}/${DOCKER_IMAGE}:${DOCKER_IMAGE_BUILD} ${DOCKER_REGISTRY}/${DOCKER_IMAGE}:${NORMALIZED_DOCKER_IMAGE_TAG}

push:
	docker push ${DOCKER_REGISTRY}/${DOCKER_IMAGE}:${NORMALIZED_DOCKER_IMAGE_TAG}

conf: 
	cp config/database.yml.sample config/database.yml
	cp config/config.yml.sample config/config.yml
