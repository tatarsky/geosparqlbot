require 'bundler/setup'

require './lib/geo_bot/helpers'
require './lib/geo_bot/services'

require './lib/geo_bot/configurator.rb'
require './lib/geo_bot/root.rb'
require './lib/geo_bot/request.rb'

module GeoBot
  class ApiError < StandardError; end
end
