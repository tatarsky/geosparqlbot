require 'logger'
require 'yaml'
require 'erb'

module GeoBot
  class Configurator
    def configure
      setup_i18n
    end

    def token
      parsed_config.fetch('telegram_bot_token')
    end

    def logger
      @logger ||= Logger.new(STDOUT, Logger::DEBUG)
    end

    def api_default_host
      parsed_config.fetch('api_default_host')
    end

    private

    def parsed_config 
      template = ERB.new (IO.read('config/config.yml'))
      @parsed_config ||= YAML::load template.result
    end

    def setup_i18n
      I18n.load_path = Dir['config/locales.yml']
      I18n.locale = :ru
      @setup_i18n ||= I18n.backend.load_translations
    end
  end
end
