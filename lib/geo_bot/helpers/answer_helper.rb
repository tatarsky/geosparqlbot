module GeoBot
  module Helpers
    module Answer
      def answer_with_text(message, bot, text)
        bot.api.send_message(chat_id: message.from.id, text: text)
      end

      def answer_with_inline_answers(message, bot, text, answers)
        kb = answers.map do |answer|
          Telegram::Bot::Types::InlineKeyboardButton.new(answer)
        end

        markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
        bot.api.send_message(chat_id: message.from.id, text: text, reply_markup: markup)
      end
    end
  end
end
