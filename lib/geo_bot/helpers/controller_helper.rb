module GeoBot
  module Helpers
    module Controller
      @already_matched = false

      def on regex, &block
        query = message.try(:text) || message.try(:data)
        regex =~ query

        if $~ && !@already_matched
          @already_matched = true
          case block.arity
          when 0
            yield
          when 1
            yield $1
          when 2
            yield $1, $2
          end
        end
      end
    end
  end
end
