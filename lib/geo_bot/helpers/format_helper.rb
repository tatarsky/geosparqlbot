module GeoBot
  module Helpers
    module Format
      def hash_to_answers(hash)
        hash.map do |k,v|
          { text: v, callback_data: k }
        end
      end

      def format_commands_list(user, cmds)
        cmds.map do |k,v|
          { text: v, callback_data: k }
        end
      end

      def format_user_devices_list(user, devices, prefix="/")
        devices.map do |dev|
          { text: "#{dev[:model]}", callback_data: "#{prefix}_#{dev[:uid]}" }
        end
      end
    end
  end
end
