require 'faraday'

module GeoBot
  class Request
    def initialize(options = {})
      host = Configurator.new.api_default_host
      @http = Faraday.new(url: host) do |c|
        c.request :url_encoded
        c.response :logger
        c.adapter  Faraday.default_adapter
      end
    end

    def get(path, params = {})
      process_response @http.get(path, params)
    end

    def post(path, params = {})
      process_response @http.post(path, params)
    end

    def post_json(path, params, headers = {})
      result = @http.post(path) do |req|
        req.headers['Content-Type'] = 'application/json'
        req.headers = req.headers.merge(headers)
        req.body = params.to_json
      end

      process_response(result)
    end

    private

    def process_response(response)
      if response.status < 300
        json = JSON.parse(response.body, symbolize_names: true)
        { status: response.status, body: json }
      else 
        {}
      end
    end
  end
end
