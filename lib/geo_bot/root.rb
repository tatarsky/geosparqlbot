require 'i18n'

module GeoBot
  class Root
    include Helpers::Controller
    include Helpers::Answer
    include Helpers::Format

    attr_reader :message
    attr_reader :bot
    attr_reader :user

    def initialize(options)
      @bot = options[:bot]
      @message = options[:message]
      @gse = Services::GeoSparqlExecutor.new
    end

    def respond
      case message
      when Telegram::Bot::Types::Message
        chat_id = @message.chat ? @message.chat.id : @message.from.id

        if message.location.instance_of? Telegram::Bot::Types::Location
          lat = message.location.latitude
          lon = message.location.longitude
          result = @gse.find lat, lon

          answer_with_text(@message, @bot, result)
        else
          answer_with_text(@message, @bot, "Message not recognized")
        end
      end
    end
  end
end
