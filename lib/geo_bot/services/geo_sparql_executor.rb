require 'sparql/client'

module GeoBot
  module Services
    class GeoSparqlExecutor

    	DEVIATION = 1.0

  		def initialize
				@sparql = SPARQL::Client.new("http://query.wikidata.org/sparql", :method => :get)
				@factforge = SPARQL::Client.new("http://factforge.net/repositories/ff-news", :method => :post)
  		end

			def find(lat, lon)
				result = []
				lighthouses = @sparql.query(query(lat, lon))

				return "Empty response" if lighthouses.empty?

				lighthouses.each do |lh|
					result << {
						image: lh[:image].to_s,
						label: lh[:itemLabel].to_s,
						abstract: find_abstract(lh[:itemLabel].to_s)
					}
				end

				prepare_result result
			end

			def find_abstract(name)
				abstract = @factforge.query(factforge_query(name))
				return "" if abstract.empty?

				abstract.first[:abstract].to_s
			end

			def prepare_result(data)
				data.map do |lt|
					"#{lt[:label]}\n#{lt[:image]}\n#{lt[:abstract]}"
				end.join("\n\n")
			end

			def query(lat, lon)
				sparql = <<-SPARQL.chomp
					PREFIX foaf: <http://xmlns.com/foaf/0.1/>

					SELECT DISTINCT ?item ?image ?coords_ ?itemLabel
					WHERE {
					  ?item wdt:P31 wd:Q39715 ;
					        wdt:P17 wd:Q159 ;
					        p:P625/psv:P625 ?coords.

					  ?coords wikibase:geoLatitude ?lat ;
					          wikibase:geoLongitude ?lon .
					  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
					  FILTER(
					    ?lat < #{prepare(lat)[0]} && 
							?lat > #{prepare(lat)[1]} && 
							?lon < #{prepare(lon)[0]} && 
							?lon > #{prepare(lon)[1]}
					  ) .
					  OPTIONAL { ?item wdt:P18 ?image }
					  BIND(strdt(concat(str(?lon), ",", str(?lat)), geo:wktLiteral) as ?coords_)
					  FILTER ( STR(?image) != '')
					}
					LIMIT 10
				SPARQL

				puts sparql

				sparql
			end

			def factforge_query(name)
				<<-SPARQL.chomp
						PREFIX dbo: <http://dbpedia.org/ontology/>
						PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
						PREFIX foaf: <http://xmlns.com/foaf/0.1/>

						SELECT ?abstract WHERE {
						   ?item foaf:name ?name.
						   ?item dbo:abstract ?abstract.
						   ?item rdf:type dbo:Lighthouse 
						   FILTER (?name = '#{name}'@en)
						}
						LIMIT 1
				SPARQL
			end

			private

			def prepare(val)
				[(val + DEVIATION).ceil(4),	(val - DEVIATION).ceil(4)]
			end
		end
	end
end